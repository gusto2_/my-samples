#!/bin/bash 

ProjectName="ldev-process-2"

# check if env and buildnumner is defined, if not, use def values
if [ -z "$ENV" ]; then
  echo "ENV is not defined, using dev"
  ENV=dev
else
  echo "ENV: ${ENV}"
fi

BuildNum=${bamboo_buildNumber}
if [ -z "${BuildNum}" ]; then
  BuildNum=$(date +"%s")
  echo "BuildNum is not defined, using ${BuildNum}"
else
  echo "BuildNum: ${BuildNum}"
fi

#TBD: code bucket as a param
#codeRepo=lead-ehub-code-artefacts-${ENV}
#codeKey="${ProjectName}/lambda-code.zip"
if [ -z "${codeRepo}" ]; then
  codeRepo=codepipeline-eu-central-1-742636525473
fi
codeKey="${ProjectName}/${ENV}-lambda-code-${BuildNum}.zip"


# deploy
echo "Copying lambda archive to s3://${codeRepo}/${codeKey}"
aws s3 cp build-deploy/lambda-code.zip s3://${codeRepo}/${codeKey}
aws s3 cp state/devstatemachine.json  s3://${codeRepo}/${ProjectName}/${ENV}-devstatemachine-${BuildNum}.json

echo "check if stack already exist"
output=$(aws cloudformation list-stacks --stack-status-filter CREATE_COMPLETE UPDATE_COMPLETE ROLLBACK_COMPLETE --query "Stacks[?StackName==\"${ProjectName}-${ENV}\"].StackName" --output text )
echo "check output: $output"

# if [ "$output" ==  'None' ]; then
#   echo "Stack ${ProjectName} does not exist and will be created"
#   action='create-stack'
#   wait_action='stack-create-complete'
# else
  echo "Stack ${ProjectName} exists and will be updated"
  action='update-stack'
  wait_action='stack-update-complete'
# fi

aws cloudformation ${action} \
  --stack-name ${ProjectName}-${ENV} \
  --template-body file://./cloudformations.yaml \
  --parameters \
    ParameterKey=Env,ParameterValue="${ENV}" \
    ParameterKey=BuildNum,ParameterValue="${BuildNum}" \
  --tags="[{\"Key\": \"Project\",\"Value\": \"LeadMgmt2\"},{\"Key\": \"Application\",\"Value\": \"Ehub\"},{\"Key\": \"Env\",\"Value\": \"${ENV}\"}]" \
  --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM CAPABILITY_AUTO_EXPAND

echo "Waiting for stack ${ProjectName}-${ENV} status ${wait_action}"
aws cloudformation wait ${wait_action}  --stack-name ${ProjectName}-${ENV}

