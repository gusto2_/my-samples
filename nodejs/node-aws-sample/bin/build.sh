#!/bin/bash 

ProjectName="l-process-2"

# check if env and buildnumner is defined, if not, use def values
if [ -z "$ENV" ]; then
  echo "ENV is not defined, using dev"
  ENV=dev
else
  echo "ENV: ${ENV}"
fi

npm install
mkdir -p build-deploy
echo "creating an archive"
zip -rq build-deploy/lambda-code.zip src/* node_modules

