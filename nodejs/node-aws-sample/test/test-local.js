const async = require('async');
const _ = require('lodash');

// const handler = require('../src/index').handler;
const assert = require('assert');
var uuidv4 = require('uuid').v4;

const testFn = function(text, callback) {
    console.log('[test] value', text);
    if(text === 'err') {
        callback(new Error("text is err"));
    } else {
      callback(null, text);
    }
}

function checkData(data) {
    if(data === undefined) {
        console.log("[checkData] undefined");
    } else if (data==null) {
        console.log("[checkData] null");
    } else {
        console.log("[checkData] data: ", data);
    }
    
    if(data) {
        console.log("[checkData] check2: ", data);
    } else {
        console.log("[checkData] check2 - no data");
    }
}

async function testAsync() {
    console.log("[testAsync] starting");
    // var items = ['abc', 'err', '123'];
    var p = await new Promise( (resolve, reject) => {
        
    // async.waterfall( [
    //         (next) => { next(null, '123'); },
    //         testFn,
    //         (data, next) => { 
    //             console.log("[testAsync] debug ", data); 
    //             next(null, data);
    //         }
    //         ], 
    //         (err, data) => {
    //             console.log('testAsync data: ' + data);
    //             console.log('testAsync err: ' + err);
                
    //             if(err === undefined || err === null) {
    //                 console.log('testAsync data: ' + data);
    //                 resolve(data);
    //             } else {
    //                 console.log('testAsync err: ' + err);
    //                 resolve(err);
    //             }
    //         }
    //     ); 
       let items = ['123', 'abc', 'errx'];
       async.each(items, (item, callback) => {
           console.log(item);
           
           async.waterfall(
               [
                   (next) => { next(null, item); },
                   testFn
               ],
               (err,data) => {
                   console.log("[testAsync] waterfall end ", err, data);
                   return callback(err, data);
               }
           );
           
           
           
       },
            (err, data) => {
                console.log('testAsync each  : ', err, data);

                if(err == null) {
                    console.log('testAsync data: ' + data);
                    resolve(data);
                } else {
                    console.log('testAsync err: ' + err);
                    reject(err);
                }
            }
       );
    
    })
    .then(data => {
        console.log('[testAsync] promise resolve: ', data);
        checkData(null);
    })
    .catch(err => {
        console.log('[testAsync] promise reject: ',err);
    });
    
    return p;
}



function testHashArray () {
    
    let message={
        MessageId: 'f5e459fe-e40b-487e-855e-0504cc2440c1', 
        ReceiptHandle: 'AQEBGHEErPmS+0lmNWS4v60UgVnZgYTL9IVVTdXKz3RtlMy+XhYU972eMcWaLW1mJfRLw4u1MnDHr+DfJLq3mDtQAdFiZhJ2jD66DyQBaxqB9QaCQL4lAFOLKGYzfe/ds4eSrwJ1fgrtgyghSiuye3d/z/cB0xyR/m+uiPZ/HEqr8SxEnQwEqATTKXo6kD7L7KpHwQ30E65xbqr1olLjWrLgfdMpxohIuEYd9beaTSAciIuP7gf1+xWG+32MmOQUbtZUeQR2MAIxrsUcy8t5989E578JVw1LZLtwumOl3n9wbSVbKUURMgtPkZkiv/1w+PHnDXc50oqJTXILAGIRtG89vXApLBkCme9nGTYoGFhRxbd6mhvacMpPf3n96/l5mPuGXmVnTNNQYNn4zuv5ASIctAeJ36Xjxg5AIT+w8Z/6lBQ=', 
        MD5OfBody: 'aecd54e32a35aa990c51416949a9aeb2', 
        Body: '{"Records":[{"EventSource":"aws:sns","EventVersion":"1.0","EventSubscriptionArn":"arn:aws:sns:eu-west-1:612927506953:test-event-notifications-toy-tes-prev:a2c4fe25-aeee-4e1d-8812-ad192d2c3ee6","Sns":{"Type":"Notification","MessageId":"0fd7cb1b-4cb5-5d86-814a-913b7fe561df","TopicArn":"arn:aws:sns:eu-west-1:612927506953:test-event-notifications-toy-tes-prev","Subject":"Amazon S3 Notification","Message":"{\\"Records\\":[{\\"eventVersion\\":\\"2.1\\",\\"eventSource\\":\\"aws:s3\\",\\"awsRegion\\":\\"eu-west-1\\",\\"eventTime\\":\\"2021-11-30T14:11:27.059Z\\",\\"eventName\\":\\"ObjectCreated:Put\\",\\"userIdentity\\":{\\"principalId\\":\\"AWS:AIDAY5NKTRIEXDLWRNSS5\\"},\\"requestParameters\\":{\\"sourceIPAddress\\":\\"10.106.132.234\\"},\\"responseElements\\":{\\"x-amz-request-id\\":\\"7PX0M4KP98PHA8TJ\\",\\"x-amz-id-2\\":\\"WcUVKKUrr5doMtALP4XAPQ8pJLq/gRDcriYajW0IQuvj1IKDa5XGUJOcnXfbqL7Bhlwc+8cZFN89lG3l9eCJ64zFmwoYao1K\\"},\\"s3\\":{\\"s3SchemaVersion\\":\\"1.0\\",\\"configurationId\\":\\"bfae1ab9-35f5-4e82-a310-1ff86a3f12be\\",\\"bucket\\":{\\"name\\":\\"test-events-xxxx-tes-prev\\",\\"ownerIdentity\\":{\\"principalId\\":\\"A2AW3HT7ZIKFC9\\"},\\"arn\\":\\"arn:aws:s3:::test-events-xxx-tes-prev\\"},\\"object\\":{\\"key\\":\\"tests/2021-11-30/26fb2b5f-76f2-401e-80bf-5cd45c5e16e0.json\\",\\"size\\":2435,\\"eTag\\":\\"552d38fac6c6447c828f17d834963413\\",\\"sequencer\\":\\"0061A6310F0765DC44\\"}}}]}","Timestamp":"2021-11-30T14:11:28.595Z","SignatureVersion":"1","Signature":"oUllcTW2DF6K6JnYrsLGMFd2yUwtMB5uwwR7EFfRu40t+w6zGRpnnwj+aqKRky5w7/p6+Ppx7elRZ6q/n3sSfJmNWt/v8Z0hY/s97mgi3DFFkPdGERCy/0Gc76a6sySe5pZ+Jwa13npO0et7h/IZf4g75YNwyRzImLwtwt1/FgXEd5hufZZnDR3GvrEaab+LCBpfnWHezaVnIsEph0cqsls0uyWYq2uqPtkSpLESgtQZomBfRlh0vE8NzD64Y/8ZLypmfkB+itayoV7UMqyr9x+Q71giMl7eL8FSUsyrj9inXbd98L+IsI9Z3Nij/vFxpIio18vbb6WarYrpLwq5eA==","SigningCertUrl":"https://sns.eu-west-1.amazonaws.com/SimpleNotificationService-7ff5318490ec183fbaddaa2a969abfda.pem","UnsubscribeUrl":"https://sns.eu-west-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:eu-west-1:612927506953:test-event-notifications-toy-tes-prev:a2c4fe25-aeee-4e1d-8812-ad192d2c3ee6","MessageAttributes":{}}}]}', MD5OfMessageAttributes: '6fee7c7d2cbc4c684de3400c47e7e87a', MessageAttributes: { ErrorMessage: { StringValue: '[object Object]', StringListValues: [], BinaryListValues: [], DataType: 'String' }, RequestID: { StringValue: '96635347-0760-4eeb-89dc-d5ce19f305d0', StringListValues: [], BinaryListValues: [], DataType: 'String' } } };

    let messageBody = JSON.parse(message.Body);
    let snsMessage = _.get(messageBody, ['Records', 0,'Sns','Message']);
    if(snsMessage) {
        snsMessage = JSON.parse(snsMessage);
        let s3Key  = _.get(snsMessage,['Records',0,'s3','object','key']);
        let bucket = _.get(snsMessage,['Records',0,'s3','bucket','name']);    
        console.log('bucket: ', bucket);
        console.log('s3key: ',  s3Key);
    } else {
        console.log("No SNS message");
    }
 
}

// console.log(test);
testAsync()
 .then( () => {
    testHashArray();     
 });


