const expect = require('chai').expect;
const assert = require('assert');

const AWS_MOCK = require('aws-sdk-mock');
const AWS = require('aws-sdk');


var uuidv4 = require('uuid').v4;

const lambdaFn = require('../src/index');
const MOCK_IDENTITY = "MOCKACC:MOCKUSER";



it('Must return identity of "MOCKACC:MOCKUSER', async () => {
  // expect(true).to.be.true;
  
  /*
  
const getUser = require('../index').getUser;
const response = require('./response');

describe('Get User tests', () => {
  beforeEach(() => {
    nock('https://api.github.com')
      .get('/users/octocat')
      .reply(200, response);
  });

  it('Get a user by username', () => {
    return getUser('octocat')
      .then(response => {
        //expect an object back
        expect(typeof response).to.equal('object');

        //Test result of name, company and location for the response
        expect(response.name).to.equal('The Octocat')
        expect(response.company).to.equal('GitHub')
        expect(response.location).to.equal('San Francisco')
      });
  });
});
  */
  AWS_MOCK.setSDKInstance(AWS);

  AWS_MOCK.mock('STS', 'getCallerIdentity', function (params, callback){
   const mockResp = {"ResponseMetadata":
     {"RequestId": uuidv4()},
     "UserId":MOCK_IDENTITY,
     "Account":"MOCKACC",
     "Arn":"arn:aws:sts::MOCKACC:assumed-role/LambdaPermission"}
    callback(null, mockResp);
  });
  
  var response = await lambdaFn.handler({});
  
  AWS_MOCK.restore('STS');
  
    assert.ok(response!=null);
    expect(response!=null).to.be.true;
    expect(response.UserId).to.equal(MOCK_IDENTITY);
});

