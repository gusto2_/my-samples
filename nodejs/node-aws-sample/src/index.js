
const AWS = require('aws-sdk');
//const STS = new AWS.STS();
const VERSION="1.3";

exports.handler = async (event) => {
    
    try {
      console.log('Received event:', JSON.stringify(event, null, 2));
    
    // var response = await createResponse(event, "received");
    
        event.Records.forEach(record => {
            var s3Records = JSON.parse(record?.Sns?.Message);
            s3Records?.Records?.forEach( s3notif => {
                console.log('S3 event:', JSON.stringify(s3notif,null,2));
                var s3Bucket = s3notif.s3.bucket.name;
                var s3Key = s3notif.s3.object.key;
                console.log('S3 object:', s3Bucket+"/"+s3Key);
            }  );
        });
        

    //   const STS = new AWS.STS();
    //   var response = await STS.getCallerIdentity({}).promise();
    //   console.log(response);
    //   response.timestamp = Date.now();
    //   response.version = VERSION;
    //   return response;
      
     // PoC processing S3 objects
    //  if (event.Records === undefined) {
    //     console.log("No Records property found");
    //     return { "status": "Error", "detail":"No Records property found" };
    //  }
     
    //  for (const record of event.Records) {
    //      console.log("received object: bucket:",record.s3.bucket.name, " key:", record.s3.object.key);
    //  }
      
      
      
    } catch(err) {
        console.log("ERROR: ", err.errorMessage);
        return { "status": "Error", "detail": err.errorMessage };
    }
};

/*
const createResponse = async (event, status) => {
    var response = {
        "event": event.id,
        "status": status
    };
    return response; 
}*/
