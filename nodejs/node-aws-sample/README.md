# Examples for NodeJS code and test cases

## Build and deploy

```
ENV=dev
# codeRepo is a bucket where the code is deployed
codeRepo=codepipeline-eu-central-1-742636525473

make clean
make build
make deploy
```



# Test cases


ref: https://scotch.io/tutorials/nodejs-tests-mocking-http-requests



```
npm init
# npm install --save aws-sdk uuid
npm install --save-dev mocha chai nock assert aws-sdk-mock

aws ec2 describe-network-interfaces --filters "Name=group-id,Values=sg-05adf06ecd94e678" "Name=subnet-id,Values=subnet-0e7adbcf5a8b5b811,subnet-0b4fef199a9c7b510"
https://policysim.aws.amazon.com/home/index.jsp?#

aws cloudformation update-stack --stack-name elearn-infra-dev --template-body file://./elearn-infra-cf.yaml
aws cloudformation create-stack --stack-name elearn-infra-dev --template-body file://./elearn-infra-cf.yaml
aws cloudformation validate-template --template-body file://./elearn-infra-cf.yaml

aws cloudformation validate-template --template-body file://./cloudformations.yaml
aws cloudformation create-stack --stack-name ldev-process-2 --template-body file://./cloudformations.yaml --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM
aws cloudformation update-stack --stack-name ldev-process-2-dev --template-body file://./cloudformations.yaml --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM \
  --parameters \
    ParameterKey=BuildNum,ParameterValue="2" 
    
   

bamboo_buildNumber=2 ./bin/deploy.sh

```

