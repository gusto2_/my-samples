# Working with Kafka 

## Install  docker compose

sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

docker-compose version


## Start Kafka

https://developer.confluent.io/faq/apache-kafka/install-and-run/#install-and-run-how-do-i-run-kafka-on-docker

https://hub.docker.com/r/bitnami/kafka


sudo yum install docker -y
sudo systemctl enable docker 
sudo systemctl start docker 


mkdir kafka
cd kafka

sudo docker-compose -f kafka-server-simple.yaml up -d
sudo docker logs -f kafka-kafka-1

### Create Kafka topic
sudo docker exec -ti kafka-kafka-1 kafka-topics.sh --bootstrap-server localhost:9092 --list
sudo docker exec -ti kafka-kafka-1 kafka-topics.sh --bootstrap-server localhost:9092 --topic FirstTopic --create --partitions 1 --replication-factor 1

### Send test payloads
sudo docker exec -ti kafka-kafka-1 kafka-console-producer.sh --bootstrap-server localhost:9092 --topic FirstTopic
sudo docker exec -ti kafka-kafka-1 kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --all-groups
sudo docker exec -ti kafka-kafka-1 kafka-consumer-groups.sh --bootstrap-server localhost:9092 --reset-offsets --to-earliest

## Configure TLS

https://jaehyeon.me/blog/2023-07-06-kafka-development-with-docker-part-9/#


