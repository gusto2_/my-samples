# RabbitMQ on Kubernetes

RabbitMQ cluster with the autocluster plugin on Kubernetes

Based on:

 - https://github.com/rabbitmq/rabbitmq-autocluster#dns-configuration
 - https://github.com/nanit/kubernetes-rabbitmq-cluster
 - https://github.com/rabbitmq/rabbitmq-peer-discovery-k8s  



Enable HA-POLICY


Replicate to all:

```
kubectl -n rabbitmq-ns exec -ti pod/rabbitmq-0 -- rabbitmqctl set_policy ha-all "" '{"ha-mode":"all","ha-sync-mode":"automatic"}'
```

Replicate to 2 nodes:

```
kubectl -n rabbitmq-ns exec -ti pod/rabbitmq-0 -- rabbitmqctl set_policy ha-all "" '{"ha-mode": "exactly", "ha-params": 2,  "ha-sync-mode":"automatic"}'
```

RabbitMQ user management

```
rabbitmqctl add_user myUser myPass
rabbitmqctl set_user_tags myUser administrator
```
