-- workaround for https://github.com/passbolt/passbolt_docker/issues/103

ALTER USER root@'%' IDENTIFIED WITH mysql_native_password BY 'root'; 
ALTER USER root@'localhost' IDENTIFIED WITH mysql_native_password BY 'root'; 
FLUSH PRIVILEGES;

