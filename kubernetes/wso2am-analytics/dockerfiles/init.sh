#!/bin/bash

if [ -d "/home/wso2carbon/wso2-conf-volume" ]; then
 echo "wso2-conf-volume found, copying configuration"
 cp -rL /home/wso2carbon/wso2-conf-volume/* ${CARBON_HOME}
else
 echo "wso2-conf-volume not found"
fi

mkdir -p /home/wso2carbon/.java/.systemPrefs

if [ -z "${JAVA_OPTS}"]; then
  echo "setting JAVA_OPTS=-Djava.util.prefs.userRoot=/home/wso2carbon/ -Djava.util.prefs.systemRoot=/home/wso2carbon/.java/"
  export JAVA_OPTS="-Djava.util.prefs.userRoot=/home/wso2carbon/ -Djava.util.prefs.systemRoot=/home/wso2carbon/.java/"
fi

if [ -n "${CARBON_PASSWORD}" ]; then
  echo -n "${CARBON_PASSWORD}" > ${CARBON_HOME}/password-tmp ;
else
  echo "CARBON_PASSWORD not present";
fi

sh ${CARBON_HOME}/bin/worker.sh $*

