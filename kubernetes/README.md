# Kubernetes examples

## Installing minikube on AWS EC2

Based on https://www.radishlogic.com/kubernetes/running-minikube-in-aws-ec2-ubuntu/

 1. Run AWS Linux2 instance

 2. Install kubectl

```
curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
chmod +x ./kubectl
sudo mv ./kubectl /usr/local/bin/kubectl
```

 3. Install Docker 

```
sudo amazon-linux-extras install -y docker
# sudo yum install -y docker # for CentOS/RHEL
sudo systemctl enable docker
sudo systemctl start  docker
```

 4. Install minikube

```
curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
chmod +x minikube
sudo mv minikube /usr/local/bin/
minikube version
```

 5. Start minikube

If /usr/local/bin/ is not in sudo's path:

```
sudo -i
echo 'export PATH=${PATH}:/usr/local/bin/' >> /root/.bashrc
source /root/.bashrc
minikube version
```


```
sudo -i
minikube start --vm-driver=none
minikube status
```

## Enable ingress on minikube

```
minikube addons enable ingress
```

