# Ingeress controller with mutual ssl

based on 

  - https://v1-14.docs.kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/


Create demo services:

```
kubectl apply -f 01-demo-deployment.yaml

```

Enable the ingress addon

```
minikube addons enable ingress

```


Define and test routes

```
kubectl apply -f 02-demo-ingress.yaml

curl -H 'host: demo1.test.local' http://localhost/
curl -H 'host: demo1.test.local' http://localhost/


```

TLS configuration


```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ssl/server.key -out ssl/server.cer -subj "/CN=*.test.local/O=test"
kubectl -n demo1 create secret tls demo1tls --key ssl/server.key --cert ssl/server.cer
kubectl -n demo2 create secret tls demo2tls --key ssl/server.key --cert ssl/server.cer
kubectl -n demo2 create secret generic trusted-ca --from-file=ca.crt=ssl/server.cer

```


Resources

 - https://docs.microsoft.com/en-us/azure/aks/ingress-tls
 - https://medium.com/@shawnlu_86806/application-gateway-work-with-aks-via-ssl-9894e7f4a587

