# Vue application

Install Vue cli

```
# a very simple single page app
vue init simple 02-vue-spa-01

# a webpack single page app
vue init webpack-simple 02-vue-spa-02
cd 02-vue-spa-02
npm install
# possible to set --host 0.0.0.0 in package.json
npm run dev
```

