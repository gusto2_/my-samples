# Start with Vue

## Create Vue app skeleton

Install nodejs and dev tools (CentOS)

```
curl -sL https://rpm.nodesource.com/setup_10.x | sudo bash 
sudo yum install -y nodejs
```

Install dev tools

```
yum groupinstall "Development tools"
```

Install bare dev tools to deploy native npm modules

```
sudo yum install gcc-c++ make
```

Install VueJS CLI

```
sudo npm install -g @vue/cli
```

Create project

```
vue create -d -r https://registry.npmjs.org/ testapp

npm run serve
```

Download libraries for existing project from package.json

```
npm install
```

# Resources

 - https://medium.com/swlh/starting-with-vue-js-89b32325d24a
