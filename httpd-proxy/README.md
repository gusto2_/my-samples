Create a proxy 80->8080

sudo docker build -t http-proxy:latest .
sudo docker run -d --rm -p 80:80 --name http-proxy --network host http-proxy:latest


alternative:
sudo docker run -d --rm -p 80:80 -p 443:443 --network host \
 -v $(pwd)/httpd.conf:/usr/local/apache2/conf/httpd.conf \
 -v $(pwd)/testkey.pem:/etc/ssl/testkey.pem -v $(pwd)/testkey.cer:/etc/ssl/testkey.cer  \
 --name httpd-proxy httpd:latest 

