/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.gvince.jaxrs.test;

import org.apache.cxf.jaxrs.JAXRSServerFactoryBean;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author GVCBB08
 */
public class ServiceRunner {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceRunner.class);
    private static final int PORT = 8080;
    private static final String HOST_IP4 = "0.0.0.0";
//    private static final String HOST_IP6 = "[::0]";
    
    
    public static void main(String[] args) {
        
        
    try {
         
         TestService ts = new TestService();
         
         JAXRSServerFactoryBean serviceFactory = new JAXRSServerFactoryBean();
         serviceFactory.setServiceBeanObjects(ts);
         String address = "http://"+HOST_IP4+":"+PORT;
         LOGGER.info("Starting the listener on {}", address);
         serviceFactory.setAddress(address);
         serviceFactory.create();
         
//         serviceFactory = new JAXRSServerFactoryBean();
//         serviceFactory.setServiceBeanObjects(ts);
//         address = "http://"+HOST_IP6+":"+PORT;
//         LOGGER.info("Starting the listener on {}", address);
//         serviceFactory.setAddress(address);
//         serviceFactory.create();
         
         
      } catch(Exception e) {
       LOGGER.error("Error starting the service", e);
       System.exit(2);
      }
    }
}

