/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eu.gvince.jaxrs.test;

import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Gabriel
 */
@Path("/")
public class TestService {
    
    private static final String PING_RESPONSE = "Gabriel's REST service v01";
    private static final Logger LOGGER = LoggerFactory.getLogger(TestService.class);
    
    @GET @Path("/")
    public Response ping() {
        return Response.ok(PING_RESPONSE, MediaType.TEXT_PLAIN).build();
    }
    
    @POST @Path("/test")
    public Response test(@FormParam("id") String id) {
        LOGGER.info("test id={}",id);
        return Response.accepted().build();
    }
    
}

