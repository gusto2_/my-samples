# IoT Lambda test

This is an example currently used for my barking detector creating:

 - DynamoDB table (storing device, timestamp, level)
 - AWS IoT Rule storing a message to the DynamoDB table (storing data)
 - Lambda to retrieve data
 
Assumed existing items (we're reusing from other projects)

 - IoT device (with permission to invoke the rule)
 - API Gateway 

# Deploying the stack

```

CODE_BUCKET=elearn-codebucket-dev
ENV=dev
STACK_NAME=bark-detector-${ENV}
ACTION=create-stack
WAIT_ACTION='stack-create-complete'
#ACTION=update-stack
#WAIT_ACTION='stack-update-complete'

BUILDID=$(date +%s)
aws s3 cp cloudformation.yaml s3://${CODE_BUCKET}/barkdetector/cloudformation-${BUILDID}.yaml

aws cloudformation validate-template --template-url $(aws s3 presign s3://${CODE_BUCKET}/barkdetector/cloudformation-${BUILDID}.yaml)

aws cloudformation ${ACTION} --stack-name ${STACK_NAME} \
 --template-url "$(aws s3 presign s3://${CODE_BUCKET}/barkdetector/cloudformation-${BUILDID}.yaml)" \
 --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM

aws cloudformation wait ${WAIT_ACTION}  --stack-name ${STACK_NAME}

```