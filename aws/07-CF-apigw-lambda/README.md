# CF with an API Gateway and Lambda Fn using the SAM

Validate

```
aws cloudformation validate-template --template-body file://./cloudformation.yaml

```

Deploy

```
aws cloudformation create-stack --stack-name mock-apis-dev --template-body file://./cloudformation.yaml \
  --capabilities CAPABILITY_AUTO_EXPAND CAPABILITY_NAMED_IAM CAPABILITY_IAM \
  --parameters "ParameterKey=Env,ParameterValue='dev'" 

```