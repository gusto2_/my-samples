/* lambda implementing and SQS client */
const { SQSClient } = require("@aws-sdk/client-sqs");
const AWS_REGION = "eu-central-1"

const sqsClient = new SQSClient( { "region": AWS_REGION } );

const handler = async (event) => {
    console.log("event ", JSON.stringify(event, null, 2));
    
    // generate random number
    const randomNumber = Math.floor(Math.random() * 100);
    if(randomNumber<50) {
        console.log("retrying the message");
        throw new Error("retrying the message");
    } else {
        console.log("All ok");
    }
}

exports.handler = handler;