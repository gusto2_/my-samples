# AWS SAMPLES


## Working with templates

Create bucket and upload a template

```
aws s3 mb s3://bucketname
aws s3 cp ./template.yaml s3://bucketname
```

Validate a template

```
aws cloudformation validate-template --template-body file://./01-cf-vpc-simple.yaml
```

Create a stack

```
aws cloudformation create-stack --stack-name <stackname> --template-body file://<template file>
aws cloudformation create-stack --stack-name <stackname> --template-url s://bucketname/<template url>
aws cloudformation create-stack  --stack-name testvpc3 --parameters ParameterKey=BastionKeyName,ParameterValue=aws_keypair --template-body file://./01-cf-vpc-ipv6-bastion.yaml
aws cloudformation create-stack  --stack-name testvpc3 --template-body file://./01-cf-vpc-ipv6-bastion.yaml --parameters 01-cf-vpc-ipv6-bastion.json
```

Create a change-set

```
aws cloudformation create-change-set  --stack-name stackname --change-set-name change-set-name  --parameters ParameterKey=KeyName,ParameterValue=aws_keypair --template-body file://./01-cf-vpc-ipv6-bastion.yaml
```


## 01-CF-create-vpc-simple

Create a VPC with two subnets (public, private) in each availability zone.
ATM zones used are defined for eu-central-1 (Frankfurt)

```
aws cloudformation create-stack --stack-name <stackname> --template-body <s3 or file url> 
```


## 02-CF-cognito-stack/

Based on https://gist.github.com/singledigit/2c4d7232fa96d9e98a3de89cf6ebe7a5
creates a AWS Cognito config, Identity and User pool stack with email validation

Manual tasks (so far):

 - create Email SES in Virginia, Oregon or Ireland region
 - validate the email address

```
aws ses verify-email-identity --email-address admin@host.com
# check and verify the email address identity
aws ses wait identity-exists --identities "admin@host.com"
aws ses verify-email-identity --email-address email@address.com
aws ses get-send-quota
```

 - add policy to the email identity:

{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "stmt1577303362931",
            "Effect": "Allow",
            "Principal": {
                "Service": "cognito-idp.amazonaws.com"
            },
            "Action": [
                "ses:SendEmail",
                "ses:SendRawEmail"
            ],
            "Resource": "arn:aws:ses:eu-central-1:535544306598:email@address.com"
        }
    ]
}

{
    "Version": "2008-10-17",
    "Statement": [
        {
            "Sid": "stmt1576103650024",
            "Effect": "Allow",
            "Principal": {
                "Service": "cognito-idp.amazonaws.com"
            },
            "Action": [
                "ses:SendEmail",
                "ses:SendRawEmail"
            ],
            "Resource": "arn:aws:ses:eu-west-1:000044300000:identity/admin@host.com"
        }
    ]
}
```

Add the SES Arn as EmailSES parameter

Create the stack


```
aws cloudformation create-stack --stack-name cognito-test-stack --template-body file://./cognito-stack.yaml --parameters file://./cognito-stack-params.json --capabilities CAPABILITY_IAM

or

aws cloudformation create-stack --stack-name cognito-test-stack --template-body file://./cognito-stack.yaml \
 --parameters ParameterKey=AuthName,ParameterValue=cognitobasic "ParameterKey=EmailSES,ParameterValue=arn:aws:ses:eu-west-1:000044300000:identity/admin@host.com" ParameterKey=EmailFrom,ParameterValue=admin@host.com \
 --capabilities CAPABILITY_IAM	


```



Resources:
 - https://thenetworkstack.com/aws-cloudformation-create-vpc/
 - https://gist.github.com/milesjordan/d86942718f8d4dc20f9f331913e7367a

