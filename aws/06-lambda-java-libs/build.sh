#!/bin/bash 

docker run -it --rm  -u 1000 \
 -v "$(pwd):/usr/src/mymaven" \
 -v "/home/$USER/.m2:/var/maven/.m2" \
 -e MAVEN_CONFIG=/var/maven/.m2 \
 -w /usr/src/mymaven  \
 maven:11  mvn -Duser.home=/var/maven clean test package
 
cd target
cp java-basic*.jar lib
zip -rq ./java-lambda.zip lib
cd ..