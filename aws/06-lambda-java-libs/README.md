# AWS Lambda fn Java Simple with jar files

An example project based on https://github.com/awsdocs/aws-lambda-developer-guide/tree/main/sample-apps/java-basic

Note:
 - seems the zip file needs to contain the `lib` folder and all jar files are imported from the lib folder
