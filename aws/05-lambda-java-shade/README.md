# AWS Lambda fn Java Simple w/ Shader

An example project built on top of https://github.com/awsdocs/aws-lambda-developer-guide/tree/main/sample-apps/java-basic

Build using the maven docker:

```
docker run -it --rm  -u 1000 \
 -v "$(pwd):/usr/src/mymaven" \
 -v "/home/$(whoami)/.m2:/var/maven/.m2" \
 -e MAVEN_CONFIG=/var/maven/.m2 \
 -w /usr/src/mymaven  \
 maven:11  mvn -Duser.home=/var/maven clean package
```